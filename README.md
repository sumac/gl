# gemlogger.

a script, inspired by [luke smith's lb](https://github.com/lukesmithxyz/lb), for managing a gemlog. 

### usage:

```
~/gl:$ ./gl help
gemlogger: a gemlog manager script.

usage:

gl <subcommand> <options>

subcommands:

new: create a new log.
publish: publish a draft.
edit: edit a published log.
edit-draft: edit a draft.
list: list published logs.
list-drafts: lists drafts
remove: removes a published log.
remove-draft: removes a draft.
help: prints this message
```
